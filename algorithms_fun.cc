#include "algorithms_fun.hh"

#include "algorithms.hh"
#include "reduction.hh"



// Level 1

auto faxpy(double alpha, const vector_t::const_ptr& x,
           const vector_t::const_ptr& y0) ->
  vector_t::ptr
{
  auto y = boost::make_shared<vector_t>(y0->N);
  copy(*y0, *y);
  axpy(alpha, *x, *y);
  return y;
}

auto faxpy(double alpha, const block_vector_t::const_ptr& x,
           const block_vector_t::const_ptr& y0) ->
  block_vector_t::ptr
{
  if (alpha == 0.0) return fcopy(y0);
  assert(y0->str == x->str);
  auto y = boost::make_shared<block_vector_t>(y0->str);
  for (std::ptrdiff_t ib=0; ib<y->str->B; ++ib) {
    if (!x->has_block(ib)) {
      if (y0->has_block(ib)) {
        y->set_block(ib, y0->block(ib).fcopy());
      }
    } else {
      if (!y0->has_block(ib)) {
        auto ytmp = x->block(ib).fscal(0.0);
        y->set_block(ib, ytmp.faxpy(alpha, x->block(ib)));
      } else {
        y->set_block(ib, y0->block(ib).faxpy(alpha, x->block(ib)));
      }
    }
  }
  return y;
}



auto fcopy(const vector_t::const_ptr& x) -> vector_t::ptr
{
  auto y = boost::make_shared<vector_t>(x->N);
  copy(*x, *y);
  return y;
}

auto fcopy(const block_vector_t::const_ptr& x) -> block_vector_t::ptr
{
  auto y = boost::make_shared<block_vector_t>(x->str);
  for (std::ptrdiff_t ib=0; ib<y->str->B; ++ib) {
    if (x->has_block(ib)) {
      y->set_block(ib, x->block(ib).fcopy());
    }
  }
  return y;
}



auto fnrm2(const vector_t::const_ptr& x) -> double
{
  return nrm2(*x);
}

auto fnrm2(const block_vector_t::const_ptr& x) -> double
{
  std::vector<hpx::future<double>> fs;
  for (std::ptrdiff_t ib=0; ib<x->str->B; ++ib) {
    if (x->has_block(ib)) {
      fs.push_back(x->block(ib).fnrm2());
    }
  }
  struct process {
    auto operator()(const hpx::future<double>& x) const -> hpx::future<double>
    {
      return hpx::async(nrm2_process, x.get());
    }
  };
  struct combine {
    auto operator()(const hpx::future<double>& x,
                    const hpx::future<double>& y) const -> hpx::future<double>
    {
      return hpx::async(nrm2_combine, x.get(), y.get());
    }
  };
  hpx::future<double> val =
    freduce(process(), combine(), hpx::make_ready_future(nrm2_init()), fs);
  return nrm2_finalize(val.get());
}



auto fscal(double alpha, const vector_t::const_ptr& x0) -> vector_t::ptr
{
  auto x = boost::make_shared<vector_t>(x0->N);
  if (alpha != 0.0) copy(*x0, *x);
  scal(alpha, *x);
  return x;
}

auto fscal(double alpha, const block_vector_t::const_ptr& x0) ->
  block_vector_t::ptr
{
  auto x = boost::make_shared<block_vector_t>(x0->str);
  for (std::ptrdiff_t ib=0; ib<x->str->B; ++ib) {
    if (x0->has_block(ib)) {
      x->set_block(ib, x0->block(ib).fscal(alpha));
    }
  }
  return x;
}



auto fset(double alpha, const vector_t::const_ptr& x0) -> vector_t::ptr
{
  auto x = boost::make_shared<vector_t>(x0->N);
  set(alpha, *x);
  return x;
}

auto fset(double alpha, const block_vector_t::const_ptr& x0) ->
  block_vector_t::ptr
{
  auto x = boost::make_shared<block_vector_t>(x0->str);
  for (std::ptrdiff_t ib=0; ib<x->str->B; ++ib) {
    if (!x0->has_block(ib)) {
      // TODO: Avoid temporary object
      x->make_block(ib);
      x->set_block(ib, x->block(ib).fset(alpha));
    } else {
      x->set_block(ib, x0->block(ib).fset(alpha));
    }
  }
  return x;
}



// Level 2

auto faxpy(bool transa, bool transb0,
           double alpha, const matrix_t::const_ptr& a,
           const matrix_t::const_ptr& b0) -> matrix_t::ptr
{
  auto nib0 = !transb0 ? b0->NI : b0->NJ;
  auto njb0 = !transb0 ? b0->NJ : b0->NI;
  auto b = boost::make_shared<matrix_t>(nib0, njb0);
  copy(transb0, *b0, *b);
  axpy(transa, alpha, *a, *b);
  return b;
}

auto faxpy(bool transa, bool transb0,
           double alpha, const block_matrix_t::const_ptr& a,
           const block_matrix_t::const_ptr& b0) -> block_matrix_t::ptr
{
  if (alpha == 0.0) return fcopy(transb0, b0);
  const auto& istra = !transa ? a->istr : a->jstr;
  const auto& jstra = !transa ? a->jstr : a->istr;
  const auto& istrb0 = !transb0 ? b0->istr : b0->jstr;
  const auto& jstrb0 = !transb0 ? b0->jstr : b0->istr;
  assert(istrb0 == istra);
  assert(jstrb0 == jstra);
  auto b = boost::make_shared<block_matrix_t>(istrb0, jstrb0);
  std::vector<hpx::future<void>> fs;
  for (std::ptrdiff_t jb=0; jb<b->jstr->B; ++jb) {
    fs.push_back(hpx::async([=]() {
          for (std::ptrdiff_t ib=0; ib<b->istr->B; ++ib) {
            auto iba = !transa ? ib : jb;
            auto jba = !transa ? jb : ib;
            auto ibb0 = !transb0 ? ib : jb;
            auto jbb0 = !transb0 ? jb : ib;
            if (!a->has_block(iba,jba)) {
              if (b0->has_block(ibb0,jbb0)) {
                b->set_block(ib,jb, b0->block(ibb0,jbb0).fcopy(transb0));
              }
            } else {
              if (!b0->has_block(ibb0,jbb0)) {
                b->set_block(ib,jb, a->block(iba,jba).fscal(transa, alpha));
              } else {
                b->set_block(ib,jb, b0->block(ibb0,jbb0).faxpy
                             (transa, transb0, alpha, a->block(iba,jba)));
              }
            }
          }
        }));
  }
  hpx::wait_all(fs);
  return b;
}



auto fcopy(bool trans, const matrix_t::const_ptr& a) -> matrix_t::ptr
{
  auto nib = !trans ? a->NI : a->NJ;
  auto njb = !trans ? a->NJ : a->NI;
  auto b = boost::make_shared<matrix_t>(nib, njb);
  copy(trans, *a, *b);
  return b;
}

auto fcopy(bool trans, const block_matrix_t::const_ptr& a) ->
  block_matrix_t::ptr
{
  const auto& istrb = !trans ? a->istr : a->jstr;
  const auto& jstrb = !trans ? a->jstr : a->istr;
  auto b = boost::make_shared<block_matrix_t>(istrb, jstrb);
  std::vector<hpx::future<void>> fs;
  for (std::ptrdiff_t jb=0; jb<b->jstr->B; ++jb) {
    fs.push_back(hpx::async([=]() {
          for (std::ptrdiff_t ib=0; ib<b->istr->B; ++ib) {
            auto iba = !trans ? ib : jb;
            auto jba = !trans ? jb : ib;
            if (a->has_block(iba,jba)) {
              b->set_block(ib,jb, a->block(iba,jba).fcopy(trans));
            }
          }
        }));
  }
  hpx::wait_all(fs);
  return b;
}



auto fgemv(bool trans,
           double alpha, const matrix_t::const_ptr& a,
           const vector_t::const_ptr& x,
           double beta, const vector_t::const_ptr& y0) -> vector_t::ptr
{
  if (alpha == 0.0) return fscal(beta, y0);
  auto y = boost::make_shared<vector_t>(a->NI);
  if (beta != 0.0) copy(*y0, *y);
  gemv(trans, alpha, *a, *x, beta, *y);
  return y;
}

auto fgemv(bool trans,
           double alpha, const block_matrix_t::const_ptr& a,
           const block_vector_t::const_ptr& x,
           double beta, const block_vector_t::const_ptr& y0) ->
  block_vector_t::ptr
{
  if (alpha == 0.0) return fscal(beta, y0);
  const auto& istra = !trans ? a->istr : a->jstr;
  const auto& jstra = !trans ? a->jstr : a->istr;
  assert(x->str == jstra);
  assert(y0->str == istra);
  auto y = boost::make_shared<block_vector_t>(y0->str);
  std::vector<hpx::future<void>> fs;
  for (std::ptrdiff_t ib=0; ib<y->str->B; ++ib) {
    struct add {
      auto operator()(const vector_t_client& x,
                      const vector_t_client& y) const -> vector_t_client
      {
        return y.faxpy(1.0, x);
      }
    };
    fs.push_back(hpx::async([=]() {
          std::vector<vector_t_client> ytmps;
          if (beta != 0.0 && y0->has_block(ib)) {
            if (beta == 1.0) {
              ytmps.push_back(y0->block(ib));
            } else {
              ytmps.push_back(y0->block(ib).fscal(beta));
            }
          }
          for (std::ptrdiff_t jb=0; jb<x->str->B; ++jb) {
            auto iba = !trans ? ib : jb;
            auto jba = !trans ? jb : ib;
            if (a->has_block(iba,jba) && x->has_block(jb)) {
              ytmps.push_back(a->block(iba,jba).fgemv
                              (trans, alpha, x->block(jb),
                               0.0, vector_t_client()));
            }
          }
          if (!ytmps.empty()) {
            y->set_block(ib, freduce(add(), ytmps));
          }
        }));
  }
  hpx::wait_all(fs);
  return y;
}



auto fnrm2(const matrix_t::const_ptr& a) -> double
{
  return nrm2(*a);
}

auto fnrm2(const block_matrix_t::const_ptr& a) -> double
{
  std::vector<hpx::future<double>> fs;
  // TODO: Parallelize jb loop
  for (std::ptrdiff_t jb=0; jb<a->jstr->B; ++jb) {
    for (std::ptrdiff_t ib=0; ib<a->istr->B; ++ib) {
      if (a->has_block(ib,jb)) {
        fs.push_back(a->block(ib,jb).fnrm2());
      }
    }
  }
  struct process {
    auto operator()(const hpx::future<double>& x) const -> hpx::future<double>
    {
      return hpx::async(nrm2_process, x.get());
    }
  };
  struct combine {
    auto operator()(const hpx::future<double>& x,
                    const hpx::future<double>& y) const -> hpx::future<double>
    {
      return hpx::async(nrm2_combine, x.get(), y.get());
    }
  };
  hpx::future<double> val =
    freduce(process(), combine(), hpx::make_ready_future(nrm2_init()), fs);
  return nrm2_finalize(val.get());
}



auto fscal(bool trans, double alpha, const matrix_t::const_ptr& a0) ->
  matrix_t::ptr
{
  auto nia0 = !trans ? a0->NI : a0->NJ;
  auto nja0 = !trans ? a0->NJ : a0->NI;
  auto a = boost::make_shared<matrix_t>(nia0, nja0);
  if (alpha != 0.0) copy(trans, *a0, *a);
  scal(alpha, *a);
  return a;
}

auto fscal(bool trans, double alpha, const block_matrix_t::const_ptr& a0) ->
  block_matrix_t::ptr
{
  const auto& istra0 = !trans ? a0->istr : a0->jstr;
  const auto& jstra0 = !trans ? a0->jstr : a0->istr;
  auto a = boost::make_shared<block_matrix_t>(istra0, jstra0);
  if (alpha == 0.0) return a;
  std::vector<hpx::future<void>> fs;
  for (std::ptrdiff_t jb=0; jb<a->jstr->B; ++jb) {
    fs.push_back(hpx::async([=]() {
          for (std::ptrdiff_t ib=0; ib<a->istr->B; ++ib) {
            auto iba0 = !trans ? ib : jb;
            auto jba0 = !trans ? jb : ib;
            if (a0->has_block(iba0,jba0)) {
              a->set_block(ib,jb, a0->block(iba0,jba0).fscal(trans, alpha));
            }
          }
        }));
  }
  hpx::wait_all(fs);
  return a;
}



auto fset(bool trans, double alpha, const matrix_t::const_ptr& a0) ->
  matrix_t::ptr
{
  auto nia0 = !trans ? a0->NI : a0->NJ;
  auto nja0 = !trans ? a0->NJ : a0->NI;
  auto a = boost::make_shared<matrix_t>(nia0, nja0);
  set(alpha, *a);
  return a;
}

auto fset(bool trans, double alpha, const block_matrix_t::const_ptr& a0) ->
  block_matrix_t::ptr
{
  const auto& istra0 = !trans ? a0->istr : a0->jstr;
  const auto& jstra0 = !trans ? a0->jstr : a0->istr;
  auto a = boost::make_shared<block_matrix_t>(istra0, jstra0);
  if (alpha == 0.0) return a;
  std::vector<hpx::future<void>> fs;
  for (std::ptrdiff_t jb=0; jb<a->jstr->B; ++jb) {
    fs.push_back(hpx::async([=]() {
          for (std::ptrdiff_t ib=0; ib<a->istr->B; ++ib) {
            auto iba0 = !trans ? ib : jb;
            auto jba0 = !trans ? jb : ib;
            if (!a0->has_block(iba0,jba0)) {
              // TODO: Avoid temporary object
              a->make_block(ib,jb);
              a->set_block(ib,jb, a->block(ib,jb).fset(false, alpha));
            } else {
              a->set_block(ib,jb, a0->block(iba0,jba0).fset(trans, alpha));
            }
          }
        }));
  }
  hpx::wait_all(fs);
  return a;
}
// auto fset(bool trans, double alpha, const block_matrix_t::const_ptr& a0) ->
//   block_matrix_t::ptr
// {
//   const auto& istra0 = !trans ? a0->istr : a0->jstr;
//   const auto& jstra0 = !trans ? a0->jstr : a0->istr;
//   auto a = boost::make_shared<block_matrix_t>(istra0, jstra0);
//   if (alpha == 0.0) return a;
//   std::vector<hpx::future<void>> fs;
//   for (std::ptrdiff_t jb=0; jb<a->jstr->B; ++jb) {
//     struct lambda {
//       bool trans;
//       double alpha;
//       block_matrix_t::const_ptr a0;
//       block_matrix_t::ptr a;
//       std::ptrdiff_t jb;
//       lambda(bool trans,
//              double alpha,
//              const block_matrix_t::const_ptr& a0,
//              const block_matrix_t::ptr& a,
//              std::ptrdiff_t jb):
//         trans(trans), alpha(alpha), a0(a0), a(a), jb(jb)
//       {
//       }
//       void operator()() const
//       {
//         for (std::ptrdiff_t ib=0; ib<a->istr->B; ++ib) {
//           auto iba0 = !trans ? ib : jb;
//           auto jba0 = !trans ? jb : ib;
//           if (!a0->has_block(iba0,jba0)) {
//             // TODO: Avoid temporary object
//             a->make_block(ib,jb);
//             a->set_block(ib,jb, a->block(ib,jb).fset(false, alpha));
//           } else {
//             a->set_block(ib,jb, a0->block(iba0,jba0).fset(trans, alpha));
//           }
//         }
//       }
//     };
//     fs.push_back(hpx::async(lambda(trans, alpha, a0, a, jb)));
//   }
//   hpx::wait_all(fs);
//   return a;
// }



// Level 3

auto fgemm(bool transa, bool transb, bool transc0,
           double alpha, const matrix_t::const_ptr& a,
           const matrix_t::const_ptr& b,
           double beta, const matrix_t::const_ptr& c0) -> matrix_t::ptr
{
  if (alpha == 0.0) return fscal(transc0, beta, c0);
  auto nia = !transa ? a->NI : a->NJ;
  auto nja = !transa ? a->NJ : a->NI;
  auto nib = !transb ? b->NI : b->NJ;
  auto njb = !transb ? b->NJ : b->NI;
  auto nic0 = beta == 0.0 ? nia : !transc0 ? c0->NI : c0->NJ;
  auto njc0 = beta == 0.0 ? njb : !transc0 ? c0->NJ : c0->NI;
  assert(nib == nja);
  assert(nic0 == nia);
  assert(njc0 == njb);
  auto c = boost::make_shared<matrix_t>(nic0, njc0);
  if (beta != 0.0) copy(transc0, *c0, *c);
  gemm(transa, transb, alpha, *a, *b, beta, *c);
  return c;
}

auto fgemm(bool transa, bool transb, bool transc0,
           double alpha, const block_matrix_t::const_ptr& a,
           const block_matrix_t::const_ptr& b,
           double beta, const block_matrix_t::const_ptr& c0) ->
  block_matrix_t::ptr
{
  if (alpha == 0.0) return fscal(transc0, beta, c0);
  const auto& istra = !transa ? a->istr : a->jstr;
  const auto& jstra = !transa ? a->jstr : a->istr;
  const auto& istrb = !transb ? b->istr : b->jstr;
  const auto& jstrb = !transb ? b->jstr : b->istr;
  const auto& istrc0 = beta == 0.0 ? istra : !transc0 ? c0->istr : c0->jstr;
  const auto& jstrc0 = beta == 0.0 ? jstrb : !transc0 ? c0->jstr : c0->istr;
  assert(istrb == jstra);
  assert(istrc0 == istra);
  assert(jstrc0 == jstrb);
  auto c = boost::make_shared<block_matrix_t>(istrc0, jstrc0);
  std::vector<hpx::future<void>> fs;
  for (std::ptrdiff_t jb=0; jb<c->jstr->B; ++jb) {
    for (std::ptrdiff_t ib=0; ib<c->istr->B; ++ib) {
      struct add {
        auto operator()(const matrix_t_client& a,
                        const matrix_t_client& b) const -> matrix_t_client
        {
          return a.faxpy(false, false, 1.0, b);
        }
      };
      fs.push_back(hpx::async([=]() {
            auto ibc0 = !transc0 ? ib : jb;
            auto jbc0 = !transc0 ? jb : ib;
            std::vector<matrix_t_client> ctmps;
            if (beta != 0.0 && c0->has_block(ibc0,jbc0)) {
              if (beta == 1.0 && !transc0) {
                ctmps.push_back(c0->block(ibc0,jbc0));
              } else {
                ctmps.push_back(c0->block(ibc0,jbc0).fscal(transc0,beta));
              }
            }
            for (std::ptrdiff_t kb=0; kb<a->jstr->B; ++kb) {
              auto iba = !transa ? ib : kb;
              auto kba = !transa ? kb : ib;
              auto kbb = !transb ? kb : jb;
              auto jbb = !transb ? jb : kb;
              if (a->has_block(iba,kba) && b->has_block(kbb,jbb)) {
                ctmps.push_back(a->block(iba,kba).fgemm
                                (transa, transb, false,
                                 alpha, b->block(kbb,jbb),
                                 0.0, matrix_t_client()));
              }
            }
            if (!ctmps.empty()) {
              c->set_block(ib,jb, freduce(add(), ctmps));
            }
          }));
    }
  }
  hpx::wait_all(fs);
  return c;
}
