#include "defs.hh"

#include "hpxbench.hh"

#include <hpx/hpx.hpp>
#include <hpx/lcos/broadcast.hpp>

#include <algorithm>
#include <atomic>
#include <cmath>
#include <cstdlib>
#include <string>
#include <vector>



union counter_t {
  std::ptrdiff_t value;
  char padding[128];
};

std::vector<counter_t> bench_counters_;

void counter_init_()
{
  bench_counters_.resize(hpx::get_os_thread_count());
  for (auto& counter: bench_counters_) counter.value = 0;
}
HPX_PLAIN_ACTION(counter_init_);
HPX_REGISTER_BROADCAST_ACTION_DECLARATION(counter_init__action);
HPX_REGISTER_BROADCAST_ACTION(counter_init__action);
void counter_init(const std::vector<hpx::id_type>& locs)
{
  hpx::wait(hpx::lcos::broadcast(counter_init__action(), locs));
}

inline void counter_inc()
{
  const std::ptrdiff_t thread = hpx::get_worker_thread_num();
  ++bench_counters_[thread].value;
}

double counter_sum_()
{
  double sum = 0;
  for (const auto& counter: bench_counters_) {
    const double val = counter.value;
    sum += val;
  }
  return sum;
}
HPX_PLAIN_ACTION(counter_sum_);
HPX_REGISTER_BROADCAST_ACTION_DECLARATION(counter_sum__action);
HPX_REGISTER_BROADCAST_ACTION(counter_sum__action);
double counter_sum(const std::vector<hpx::id_type>& locs)
{
  const auto vals = hpx::lcos::broadcast(counter_sum__action(), locs);
  double sum = 0.0;
  for (auto val: vals.get()) sum += val;
  return sum;
}

std::ptrdiff_t counter_sum2_()
{
  double sum2 = 0;
  for (const auto& counter: bench_counters_) {
    const double val = counter.value;
    sum2 += val * val;
  }
  return sum2;
}
HPX_PLAIN_ACTION(counter_sum2_);
HPX_REGISTER_BROADCAST_ACTION_DECLARATION(counter_sum2__action);
HPX_REGISTER_BROADCAST_ACTION(counter_sum2__action);
double counter_sum2(const std::vector<hpx::id_type>& locs)
{
  const auto vals = hpx::lcos::broadcast(counter_sum2__action(), locs);
  double sum2 = 0.0;
  for (auto val: vals.get()) sum2 += val;
  return sum2;
}

std::ptrdiff_t counter_max_()
{
  double max = 0;
  for (const auto& counter: bench_counters_) {
    const double val = counter.value;
    max = std::fmax(max, val);
  }
  return max;
}
HPX_PLAIN_ACTION(counter_max_);
HPX_REGISTER_BROADCAST_ACTION_DECLARATION(counter_max__action);
HPX_REGISTER_BROADCAST_ACTION(counter_max__action);
double counter_max(const std::vector<hpx::id_type>& locs)
{
  const auto vals = hpx::lcos::broadcast(counter_max__action(), locs);
  double max = 0.0;
  for (auto val: vals.get()) max = std::fmax(max, val);
  return max;
}

void counter_check(const std::vector<hpx::id_type>& locs, std::ptrdiff_t njobs)
{
  const double count = locs.size() * hpx::get_os_thread_count();
  const double sum = counter_sum(locs);
  const double sum2 = counter_sum2(locs);
  const double max = counter_max(locs);
  const double avg = sum / count;
  const double sdv = std::sqrt(std::fmax(0.0, sum2 / count - avg*avg));
  const double nsd = sdv / avg;
  const double imb = std::fmax(0.0, max / avg - 1.0);
  std::cout << "[nsd=" << nsd << "]" << std::flush;
  std::cout << "[imb=" << imb << "]" << std::flush;
  assert(sum == njobs);
}



enum continuation_t { do_nothing,
                      do_daisychain_func, do_daisychain,
                      do_tree_func, do_tree };

void busywait(double time, continuation_t cont, std::ptrdiff_t njobs);
HPX_PLAIN_ACTION(busywait);



void run_next(double time, continuation_t cont, std::ptrdiff_t njobs)
{
  switch (cont) {
  default:
    assert(0); __builtin_unreachable();
  case do_nothing:
    break;
  case do_daisychain_func: {
    auto f = hpx::async(busywait, time, cont, njobs);
    hpx::wait(f);
    break;
  }
  case do_daisychain: {
    const auto here = hpx::find_here();
    auto f = hpx::async(busywait_action(), here, time, cont, njobs);
    hpx::wait(f);
    break;
  }
  case do_tree_func: {
    auto njobs1 = (njobs+1) / 2;
    auto njobs2 = njobs - njobs1;
    auto f1 = hpx::async(busywait, time, cont, njobs1);
    auto f2 = hpx::async(busywait, time, cont, njobs2);
    hpx::wait_all(f1, f2);
    break;
  }
  case do_tree: {
    const auto here = hpx::find_here();
    auto njobs1 = (njobs+1) / 2;
    auto njobs2 = njobs - njobs1;
    auto f1 = hpx::async(busywait_action(), here, time, cont, njobs1);
    auto f2 = hpx::async(busywait_action(), here, time, cont, njobs2);
    hpx::wait_all(f1, f2);
    break;
  }
  }
}



void busywait(double time, continuation_t cont, std::ptrdiff_t njobs)
{
  assert(njobs >= 0);
  if (njobs == 0) return;
  counter_inc();
  const auto t0 = gettime();
  for (;;) {
    const auto t1 = gettime();
    if (elapsed(t1, t0) >= time) break;
  }
  run_next(time, cont, njobs-1);
}



void run_sync_func(double time, std::ptrdiff_t njobs, std::ptrdiff_t nthreads)
{
  std::cout << "    Configuration: function sync...       " << std::flush;
  const auto here = hpx::find_here();
  const std::vector<hpx::id_type> locs(1, here);
  counter_init(locs);
  const auto t0 = gettime();
  for (std::ptrdiff_t n=0; n<njobs; ++n) {
    busywait(time, do_nothing, 1);
  }
  const auto t1 = gettime();
  counter_check(locs, njobs);
  const double usec = elapsed(t1, t0) / njobs * 1.0e+6;
  std::cout << " " << usec << " µsec" << std::endl << std::flush;
}

void run_sync(double time, std::ptrdiff_t njobs,
              const std::vector<hpx::id_type>& locs, std::ptrdiff_t nthreads)
{
  std::cout << "    Configuration: action sync...         " << std::flush;
  counter_init(locs);
  const auto t0 = gettime();
  for (std::ptrdiff_t n=0; n<njobs; ++n) {
    busywait_action()(locs[n % locs.size()], time, do_nothing, 1);
  }
  const auto t1 = gettime();
  counter_check(locs, njobs);
  const double usec = elapsed(t1, t0) / njobs * 1.0e+6;
  std::cout << " " << usec << " µsec" << std::endl << std::flush;
}

void run_single(double time, std::ptrdiff_t njobs)
{
  for (std::ptrdiff_t n=0; n<njobs; ++n) {
    busywait(time, do_nothing, 1);
  }
}
HPX_PLAIN_ACTION(run_single);

void run_multi_func(double time, std::ptrdiff_t njobs, std::ptrdiff_t nthreads)
{
  std::cout << "    Configuration: function multi...      " << std::flush;
  std::vector<hpx::future<void>> fs(nthreads);
  const auto here = hpx::find_here();
  const std::vector<hpx::id_type> locs(1, here);
  counter_init(locs);
  const auto t0 = gettime();
  auto njobs_left = njobs;
  const auto njobs1 = (njobs + nthreads - 1) / nthreads;
  for (std::ptrdiff_t t=0; t<nthreads; ++t) {
    const auto njobs2 = std::min(njobs1, njobs_left);
    njobs_left -= njobs2;
    fs[t] = hpx::async(run_single, time, njobs2);
  }
  assert(njobs_left == 0);
  hpx::wait_all(fs);
  const auto t1 = gettime();
  counter_check(locs, njobs);
  const double usec = elapsed(t1, t0) * nthreads / njobs * 1.0e+6;
  std::cout << " " << usec << " µsec" << std::endl << std::flush;
}

void run_multi(double time, std::ptrdiff_t njobs,
               const std::vector<hpx::id_type>& locs, std::ptrdiff_t nthreads)
{
  std::cout << "    Configuration: action multi...        " << std::flush;
  assert(nthreads == std::ptrdiff_t(locs.size() * hpx::get_os_thread_count()));
  std::vector<hpx::future<void>> fs(nthreads);
  counter_init(locs);
  const auto t0 = gettime();
  auto njobs_left = njobs;
  const auto njobs1 = (njobs + nthreads - 1) / nthreads;
  for (std::ptrdiff_t t=0; t<nthreads; ++t) {
    const auto njobs2 = std::min(njobs1, njobs_left);
    njobs_left -= njobs2;
    fs[t] = hpx::async(run_single_action(), locs[t % locs.size()],
                       time, njobs2);
  }
  assert(njobs_left == 0);
  hpx::wait_all(fs);
  const auto t1 = gettime();
  counter_check(locs, njobs);
  const double usec = elapsed(t1, t0) * nthreads / njobs * 1.0e+6;
  std::cout << " " << usec << " µsec" << std::endl << std::flush;
}

void run_serial_func(double time, std::ptrdiff_t njobs, std::ptrdiff_t nthreads)
{
  std::cout << "    Configuration: function serial...     " << std::flush;
  const auto here = hpx::find_here();
  const std::vector<hpx::id_type> locs(1, here);
  counter_init(locs);
  const auto t0 = gettime();
  for (std::ptrdiff_t n=0; n<njobs; ++n) {
    auto f = hpx::async(busywait, time, do_nothing, 1);
    hpx::wait(f);
  }
  const auto t1 = gettime();
  counter_check(locs, njobs);
  const double usec = elapsed(t1, t0) / njobs * 1.0e+6;
  std::cout << " " << usec << " µsec" << std::endl << std::flush;
}

void run_serial(double time, std::ptrdiff_t njobs,
                const std::vector<hpx::id_type>& locs, std::ptrdiff_t nthreads)
{
  std::cout << "    Configuration: action serial...       " << std::flush;
  counter_init(locs);
  const auto t0 = gettime();
  for (std::ptrdiff_t n=0; n<njobs; ++n) {
    auto f = hpx::async(busywait_action(), locs[n % locs.size()],
                        time, do_nothing, 1);
    hpx::wait(f);
  }
  const auto t1 = gettime();
  counter_check(locs, njobs);
  const double usec = elapsed(t1, t0) / njobs * 1.0e+6;
  std::cout << " " << usec << " µsec" << std::endl << std::flush;
}

void run_daisychain_func(double time, std::ptrdiff_t njobs,
                         std::ptrdiff_t nthreads)
{
  std::cout << "    Configuration: function daisychain... " << std::flush;
  const auto here = hpx::find_here();
  const std::vector<hpx::id_type> locs(1, here);
  counter_init(locs);
  const auto t0 = gettime();
  run_next(time, do_daisychain_func, njobs);
  const auto t1 = gettime();
  counter_check(locs, njobs);
  const double usec = elapsed(t1, t0) / njobs * 1.0e+6;
  std::cout << " " << usec << " µsec" << std::endl << std::flush;
}

void run_daisychain(double time, std::ptrdiff_t njobs, std::ptrdiff_t nthreads)
{
  std::cout << "    Configuration: action daisychain...   " << std::flush;
  const auto here = hpx::find_here();
  const std::vector<hpx::id_type> locs(1, here);
  counter_init(locs);
  const auto t0 = gettime();
  run_next(time, do_daisychain, njobs);
  const auto t1 = gettime();
  counter_check(locs, njobs);
  const double usec = elapsed(t1, t0) / njobs * 1.0e+6;
  std::cout << " " << usec << " µsec" << std::endl << std::flush;
}

void run_parallel_func(double time, std::ptrdiff_t njobs,
                       std::ptrdiff_t nthreads)
{
  std::cout << "    Configuration: function parallel...   " << std::flush;
  std::vector<hpx::future<void>> fs(njobs);
  const auto here = hpx::find_here();
  const std::vector<hpx::id_type> locs(1, here);
  counter_init(locs);
  const auto t0 = gettime();
  for (std::ptrdiff_t n=0; n<njobs; ++n) {
    fs[n] = hpx::async(busywait, time, do_nothing, 1);
  }
  hpx::wait_all(fs);
  const auto t1 = gettime();
  counter_check(locs, njobs);
  const double usec = elapsed(t1, t0) * nthreads / njobs * 1.0e+6;
  std::cout << " " << usec << " µsec" << std::endl << std::flush;
}

void run_parallel(double time, std::ptrdiff_t njobs,
                  const std::vector<hpx::id_type>& locs,
                  std::ptrdiff_t nthreads)
{
  std::cout << "    Configuration: action parallel...     " << std::flush;
  std::vector<hpx::future<void>> fs(njobs);
  counter_init(locs);
  const auto t0 = gettime();
  for (std::ptrdiff_t n=0; n<njobs; ++n) {
    fs[n] = hpx::async(busywait_action(), locs[n % locs.size()],
                       time, do_nothing, 1);
  }
  hpx::wait_all(fs);
  const auto t1 = gettime();
  counter_check(locs, njobs);
  const double usec = elapsed(t1, t0) * nthreads / njobs * 1.0e+6;
  std::cout << " " << usec << " µsec" << std::endl << std::flush;
}

void run_tree_func(double time, std::ptrdiff_t njobs, std::ptrdiff_t nthreads)
{
  std::cout << "    Configuration: function tree...       " << std::flush;
  const auto here = hpx::find_here();
  const std::vector<hpx::id_type> locs(1, here);
  counter_init(locs);
  const auto t0 = gettime();
  run_next(time, do_tree_func, njobs);
  const auto t1 = gettime();
  counter_check(locs, njobs);
  const double usec = elapsed(t1, t0) * nthreads / njobs * 1.0e+6;
  std::cout << " " << usec << " µsec" << std::endl << std::flush;
}

void run_tree(double time, std::ptrdiff_t njobs, std::ptrdiff_t nthreads)
{
  std::cout << "    Configuration: action tree...         " << std::flush;
  const auto here = hpx::find_here();
  const std::vector<hpx::id_type> locs(1, here);
  counter_init(locs);
  const auto t0 = gettime();
  run_next(time, do_tree, njobs);
  const auto t1 = gettime();
  counter_check(locs, njobs);
  const double usec = elapsed(t1, t0) * nthreads / njobs * 1.0e+6;
  std::cout << " " << usec << " µsec" << std::endl << std::flush;
}

void run_sqrt_func(double time, std::ptrdiff_t njobs, std::ptrdiff_t nthreads)
{
  std::cout << "    Configuration: function sqrt...       " << std::flush;
  const std::ptrdiff_t njobs_par = std::lrint(std::sqrt(njobs));
  std::vector<hpx::future<void>> fs(njobs_par);
  const auto here = hpx::find_here();
  const std::vector<hpx::id_type> locs(1, here);
  counter_init(locs);
  const auto t0 = gettime();
  auto njobs_left = njobs;
  const auto njobs1 = (njobs + njobs_par - 1) / njobs_par;
  for (std::ptrdiff_t n=0; n<njobs_par; ++n) {
    const auto njobs2 = std::min(njobs1, njobs_left);
    njobs_left -= njobs2;
    fs[n] = hpx::async(busywait, time, do_daisychain_func, njobs2);
  }
  assert(njobs_left == 0);
  hpx::wait_all(fs);
  const auto t1 = gettime();
  counter_check(locs, njobs);
  const double usec = elapsed(t1, t0) * nthreads / njobs * 1.0e+6;
  std::cout << " " << usec << " µsec" << std::endl << std::flush;
}

void run_sqrt(double time, std::ptrdiff_t njobs,
              const std::vector<hpx::id_type>& locs, std::ptrdiff_t nthreads)
{
  std::cout << "    Configuration: action sqrt...         " << std::flush;
  const std::ptrdiff_t njobs_par = std::lrint(std::sqrt(njobs));
  std::vector<hpx::future<void>> fs(njobs_par);
  counter_init(locs);
  const auto t0 = gettime();
  auto njobs_left = njobs;
  const auto njobs1 = (njobs + njobs_par - 1) / njobs_par;
  for (std::ptrdiff_t n=0; n<njobs_par; ++n) {
    const auto njobs2 = std::min(njobs1, njobs_left);
    njobs_left -= njobs2;
    fs[n] = hpx::async(busywait_action(), locs[n % locs.size()],
                       time, do_daisychain, njobs2);
  }
  assert(njobs_left == 0);
  hpx::wait_all(fs);
  const auto t1 = gettime();
  counter_check(locs, njobs);
  const double usec = elapsed(t1, t0) * nthreads / njobs * 1.0e+6;
  std::cout << " " << usec << " µsec" << std::endl << std::flush;
}



void hpxbench_local_functions()
{
  const std::ptrdiff_t njobs = 1000;
  const std::ptrdiff_t nthreads = hpx::get_os_thread_count();
  std::cout << "HPX local function benchmarks: "
            << "N=" << njobs << ", T=" << nthreads << std::endl;
  std::vector<hpx::id_type> locs(1);
  locs[0] = hpx::find_here();
  
  const double times[] = {0.0, 10.0e-6, 100.0e-6};
  
  for (auto time: times) {
    std::cout << "  Benchmark: busywait (" << time * 1.0e+6 << " µsec)"
              << std::endl;
    run_sync_func(time, njobs, nthreads);
    run_multi_func(time, njobs, nthreads);
    run_serial_func(time, njobs, nthreads);
    run_daisychain_func(time, njobs, nthreads);
    run_parallel_func(time, njobs, nthreads);
    run_tree_func(time, njobs, nthreads);
    run_sqrt_func(time, njobs, nthreads);
  }
  
  std::cout << std::endl << std::flush;
}

void hpxbench_local_actions()
{
  const std::ptrdiff_t njobs = 1000;
  const std::ptrdiff_t nthreads = hpx::get_os_thread_count();
  std::cout << "HPX local action benchmarks: "
            << "N=" << njobs << ", T=" << nthreads << std::endl;
  std::vector<hpx::id_type> locs(1);
  locs[0] = hpx::find_here();
  
  const double times[] = {0.0, 10.0e-6, 100.0e-6};
  
  for (auto time: times) {
    std::cout << "  Benchmark: busywait (" << time * 1.0e+6 << " µsec)"
              << std::endl;
    run_sync(time, njobs, locs, nthreads);
    run_multi(time, njobs, locs, nthreads);
    run_serial(time, njobs, locs, nthreads);
    run_daisychain(time, njobs, nthreads);
    run_parallel(time, njobs, locs, nthreads);
    run_tree(time, njobs, nthreads);
    run_sqrt(time, njobs, locs, nthreads);
  }
  
  std::cout << std::endl << std::flush;
}

void hpxbench_global_actions()
{
  const std::ptrdiff_t njobs = 1000;
  const std::ptrdiff_t nthreads = hpx::get_num_worker_threads();
  std::cout << "HPX global action benchmarks: "
            << "N=" << njobs << ", T=" << nthreads << std::endl;
  const auto locs = hpx::find_all_localities();
  
  const double times[] = {0.0, 10.0e-6, 100.0e-6};
  
  for (auto time: times) {
    std::cout << "  Benchmark: busywait (" << time * 1.0e+6 << " µsec)"
              << std::endl;
    run_sync(time, njobs, locs, nthreads);
    run_multi(time, njobs, locs, nthreads);
    run_serial(time, njobs, locs, nthreads);
    // run_daisychain(time, njobs, nthreads);
    run_parallel(time, njobs, locs, nthreads);
    // run_tree(time, njobs, nthreads);
    run_sqrt(time, njobs, locs, nthreads);
  }
  
  std::cout << std::endl << std::flush;
}



void hpxbench()
{
  const auto t0 = gettime();
  const auto t1 = gettime();
  const double dt = elapsed(t1, t0);
  std::cout << "Timing overhead: " << dt * 1.0e+6 << " µsec" << std::endl
            << std::endl;
  
  hpxbench_local_functions();
  hpxbench_local_actions();
  if (hpx::get_num_localities().get() > 1) {
    hpxbench_global_actions();
  }
}
