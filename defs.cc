#include "defs.hh"

#include <hpx/hpx.hpp>
#include <hpx/lcos/broadcast.hpp>

#include <cstdlib>
#include <iostream>



static double timebase = 0.0;

void init_timebase_local()
{
  auto s0 = hpx::util::hardware::timestamp();
  auto t0 = hpx::get_system_uptime();
  while (hpx::get_system_uptime() < t0 + 100000000UL);
  auto s1 = hpx::util::hardware::timestamp();
  auto t1 = hpx::get_system_uptime();
  timebase = (t1 - t0) / (1.0e+9 * (s1 - s0));
}
HPX_PLAIN_ACTION(init_timebase_local)
HPX_REGISTER_BROADCAST_ACTION_DECLARATION(init_timebase_local_action);
HPX_REGISTER_BROADCAST_ACTION(init_timebase_local_action);
void init_timebase()
{
  auto locs = hpx::find_all_localities();
  auto f = hpx::lcos::broadcast(init_timebase_local_action(), locs);
  hpx::wait(f);
  std::cout << "Benchmark timer base is " << timebase * 1.0e+9 << " nsec "
            << "(" << 1.0e-9 / timebase << " GHz)"
            << std::endl
            << std::endl;
}

double elapsed(ticks t1, ticks t0)
{
  // return (t1 - t0) / 1.0e+9;
  return (t1 - t0) * timebase;
}
