#ifndef TENSOR_HH
#define TENSOR_HH

#include "matrix.hh"

#include <array>
#include <cstddef>



template<std::ptrdiff_t D>
struct tensor_t {
  std::array<std::ptrdiff_t,D> N;
  vector_t elts;
  
  tensor_t(const std::array<std::ptrdiff_t,D>& N): N(N), elts(size()) {}
  
  std::ptrdiff_t size() const
  {
    std::ptrdiff_t sz = 1;
    for (auto n: N) sz *= n;
    return sz;
  }
};

#endif  // #ifndef TENSOR_HH
