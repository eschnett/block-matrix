#ifndef DEFS_HH
#define DEFS_HH

#include <hpx/hpx.hpp>
#include <hpx/util/hardware/timestamp.hpp>

#include <cassert>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <vector>



void init_timebase();

typedef long long ticks;

inline ticks gettime()
{
  // return hpx::get_system_uptime();
  return hpx::util::hardware::timestamp();
}

double elapsed(ticks t1, ticks t0);



template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T> v)
{
  os << "[";
  for (std::size_t i=0; i<v.size(); ++i) {
    if (i != 0) os << ",";
    os << v[i];
  }
  os << "]";
  return os;
}

template<typename T>
std::string mkstr(const T& x)
{
  std::ostringstream os;
  os << x;
  return os.str();
}



#endif  // #ifndef DEFS_HH
