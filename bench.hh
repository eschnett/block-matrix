#ifndef BENCH_HH
#define BENCH_HH

#include <cstdlib>

namespace bench {
  extern std::ptrdiff_t niters;
  extern std::ptrdiff_t nsize;
  extern std::ptrdiff_t nblocks;
}

void bench_dense();
void bench_fdense();
void bench_dense_parallel();
void bench_fdense_parallel();
void bench_fblock_local();
void bench_fblock_global();

#endif  // #ifndef BENCH_HH
