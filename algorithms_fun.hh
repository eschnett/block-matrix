#ifndef ALGORITHMS_FUN_HH
#define ALGORITHMS_FUN_HH

#include "block_matrix.hh"
#include "matrix.hh"



// Level 1

auto faxpy(double alpha, const vector_t::const_ptr& x,
           const vector_t::const_ptr& y0) ->
  vector_t::ptr;
auto faxpy(double alpha, const block_vector_t::const_ptr& x,
           const block_vector_t::const_ptr& y0) ->
  block_vector_t::ptr;

auto fcopy(const vector_t::const_ptr& x) -> vector_t::ptr;
auto fcopy(const block_vector_t::const_ptr& x) -> block_vector_t::ptr;

auto fnrm2(const vector_t::const_ptr& x) -> double;
auto fnrm2(const block_vector_t::const_ptr& x) -> double;

auto fscal(double alpha, const vector_t::const_ptr& x0) -> vector_t::ptr;
auto fscal(double alpha, const block_vector_t::const_ptr& x0) ->
  block_vector_t::ptr;

auto fset(double alpha, const vector_t::const_ptr& x0) -> vector_t::ptr;
auto fset(double alpha, const block_vector_t::const_ptr& x0) ->
  block_vector_t::ptr;



// Level 2

auto faxpy(bool transa, bool transb0,
           double alpha, const matrix_t::const_ptr& a,
           const matrix_t::const_ptr& b0) -> matrix_t::ptr;
auto faxpy(bool transa, bool transb0,
           double alpha, const block_matrix_t::const_ptr& a,
           const block_matrix_t::const_ptr& b0) -> block_matrix_t::ptr;

auto fcopy(bool trans, const matrix_t::const_ptr& a) -> matrix_t::ptr;
auto fcopy(bool trans, const block_matrix_t::const_ptr& a) ->
  block_matrix_t::ptr;

auto fgemv(bool trans,
           double alpha, const matrix_t::const_ptr& a,
           const vector_t::const_ptr& x,
           double beta, const vector_t::const_ptr& y0) -> vector_t::ptr;
auto fgemv(bool trans,
           double alpha, const block_matrix_t::const_ptr& a,
           const block_vector_t::const_ptr& x,
           double beta, const block_vector_t::const_ptr& y0) ->
  block_vector_t::ptr;

auto fnrm2(const matrix_t::const_ptr& x) -> double;
auto fnrm2(const block_matrix_t::const_ptr& x) -> double;

auto fscal(bool trans, double alpha, const matrix_t::const_ptr& a0) ->
  matrix_t::ptr;
auto fscal(bool trans, double alpha, const block_matrix_t::const_ptr& a0) ->
  block_matrix_t::ptr;

auto fset(bool trans, double alpha, const matrix_t::const_ptr& a0) ->
  matrix_t::ptr;
auto fset(bool trans, double alpha, const block_matrix_t::const_ptr& a0) ->
  block_matrix_t::ptr;



// Level 3

auto fgemm(bool transa, bool transb, bool transc0,
           double alpha, const matrix_t::const_ptr& a,
           const matrix_t::const_ptr& b,
           double beta, const matrix_t::const_ptr& c0) -> matrix_t::ptr;
auto fgemm(bool transa, bool transb, bool transc0,
           double alpha, const block_matrix_t::const_ptr& a,
           const block_matrix_t::const_ptr& b,
           double beta, const block_matrix_t::const_ptr& c0) ->
  block_matrix_t::ptr;

#endif  // #ifndef ALGORITHMS_FUN_HH
