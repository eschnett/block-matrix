#include "hwloc.hh"

#include <hpx/hpx.hpp>

#include <hwloc.h>

#include <errno.h>
#include <string.h>
#include <sys/time.h>

#include <atomic>
#include <cmath>
#include <iostream>
#include <sstream>
#include <string>

#include <sys/stat.h>



static void output_affinity(std::ostream& os, const hwloc_topology_t& topology)
{
  const int pu_depth = hwloc_get_type_or_below_depth(topology, HWLOC_OBJ_PU);
  assert(pu_depth>=0);
  const int num_pus = hwloc_get_nbobjs_by_depth(topology, pu_depth);
  assert(num_pus>0);
  hwloc_cpuset_t cpuset = hwloc_bitmap_alloc();
  assert(cpuset);
  const int ierr = hwloc_get_cpubind(topology, cpuset, HWLOC_CPUBIND_THREAD);
  if (ierr) {
    os << "   Could not determine CPU bindings" << std::endl;
    hwloc_bitmap_free(cpuset);
    return;
  }
  hwloc_cpuset_t lcpuset = hwloc_bitmap_alloc();
  assert(lcpuset);
  for (int pu_num=0; pu_num<num_pus; ++pu_num) {
    const hwloc_obj_t pu_obj =
      hwloc_get_obj_by_depth(topology, pu_depth, pu_num);
    if (hwloc_bitmap_isset(cpuset, pu_obj->os_index)) {
      hwloc_bitmap_set(lcpuset, pu_num);
    }
  }
  char lcpuset_buf[1000];
  hwloc_bitmap_list_snprintf(lcpuset_buf, sizeof lcpuset_buf, lcpuset);
  hwloc_bitmap_free(lcpuset);
  char cpuset_buf[1000];
  hwloc_bitmap_list_snprintf(cpuset_buf, sizeof cpuset_buf, cpuset);
  hwloc_bitmap_free(cpuset);
  
  hpx::id_type here = hpx::find_here();
  os << "    "
     << "L" << hpx::get_locality_id() << " "
     << "(" << hpx::get_locality_name(here).get() << ") "
     << "T" << hpx::get_worker_thread_num() << " "
     << "(" << hpx::get_thread_name() << "): "
     << "PU set L#{" << lcpuset_buf << "} "
     << "P#{" << cpuset_buf << "}" << std::endl;
}

static __attribute__((__unused__))
void set_affinity(std::ostream& os, const hwloc_topology_t& topology)
{
  const int pu_depth = hwloc_get_type_or_below_depth(topology, HWLOC_OBJ_PU);
  assert(pu_depth>=0);
  const int num_pus = hwloc_get_nbobjs_by_depth(topology, pu_depth);
  assert(num_pus>0);
  const int thread = hpx::get_worker_thread_num();
  const int pu_num = thread;
  assert(pu_num < num_pus);
  hwloc_obj_t pu_obj = hwloc_get_obj_by_depth(topology, pu_depth, pu_num);
  assert(pu_obj);
  // hwloc_cpuset_t cpuset = pu_obj->cpuset;
  hwloc_cpuset_t cpuset = hwloc_bitmap_dup(pu_obj->cpuset);
  int ierr;
  ierr = hwloc_set_cpubind(topology, cpuset,
                           HWLOC_CPUBIND_THREAD | HWLOC_CPUBIND_STRICT);
  if (ierr) {
    ierr = hwloc_set_cpubind(topology, cpuset, HWLOC_CPUBIND_THREAD);
  }
  if (ierr) {
    os << "Could not set CPU binding for thread " << pu_num << std::endl;
  }
  hwloc_bitmap_free(cpuset);
}

#if 0
std::atomic<int> hwloc_count;
#endif

std::pair<int,std::string> hwloc_run()
{
  std::ostringstream os;
  const int thread = hpx::get_worker_thread_num();
#if 0
  const int nthreads = hpx::get_os_thread_count();
  // Wait until all threads are running to ensure all threads are
  // running simultaneously, and thus are running on different PUs
  assert(hwloc_count < nthreads);
  ++hwloc_count;
  while (hwloc_count < nthreads) {
    // do nothing
  }
  assert(hwloc_count == nthreads);
#endif
  hwloc_topology_t topology;
  hwloc_topology_init(&topology);
  hwloc_topology_load(topology);
  output_affinity(os, topology);
  // set_affinity(os, topology);
  // output_affinity(os, topology);
  return { thread, os.str() };
}
HPX_PLAIN_ACTION(hwloc_run);

std::string hwloc_run_on_threads()
{
  std::ostringstream os;
  int nthreads = hpx::get_os_thread_count();
  // Submit more threads that OS threads to ensure each OS thread will
  // run at least one thread
  int nsubmit = 10 * nthreads;
  auto here = hpx::find_here();
  std::vector<hpx::future<std::pair<int,std::string>>> fs;
#if 0
  hwloc_count = 0;
#endif
  for (int submit=0; submit<nsubmit; ++submit) {
    fs.push_back(hpx::async(hwloc_run_action(), here));
  }
  hpx::wait_all(fs);
#if 0
  assert(hwloc_count == nthreads);
#endif
  std::vector<std::string> strs(nthreads);
  for (const auto& f: fs) {
    auto thread = f.get().first;
    auto name = f.get().second;
    assert(thread>=0 && thread<nthreads);
    // assert(strs[thread].empty());
    strs[thread] = name;
  }
  for (const auto& s: strs) {
    assert(!s.empty());
    os << s;
  }
  return os.str();
}
HPX_PLAIN_ACTION(hwloc_run_on_threads);

static void run_on_localities()
{
  const auto locs = hpx::find_all_localities();
  std::vector<hpx::future<std::string>> fs;
  for (const auto& loc: locs) {
    fs.push_back(hpx::async(hwloc_run_on_threads_action(), loc));
  }
  for (const auto& f: fs) {
    std::cout << f.get();
  }
}

void output_hwloc_info()
{
  std::cout << "HWLOC information:" << std::endl;
  run_on_localities();
  std::cout << std::endl;
}
