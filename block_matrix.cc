#include "block_matrix.hh"

#include "matrix.hh"

#include <hpx/hpx.hpp>
#include <hpx/include/components.hpp>

#include <cassert>
#include <cstdlib>



bool structure_t::invariant() const
{
  if (N < 0) return false;
  if (B < 0) return false;
  if (begin[0] != 0) return false;
  for (std::ptrdiff_t b=0; b<B; ++b) {
    if (begin[b+1] <= begin[b]) return false;
  }
  if (begin[B] != N) return false;
  return true;
}

std::ptrdiff_t structure_t::find(std::ptrdiff_t i) const
{
  assert(i>=0 && i<N);
  std::ptrdiff_t b0 = 0, b1 = B;
  auto loopinv = [&]() { return b0>=0 && b1<=B && b0<b1; };
  auto loopvar = [&]() { return b1 - b0; };
  assert(loopinv());
  std::ptrdiff_t old_loopvar = loopvar();
  while (b0+1 < b1 && i>=begin[b0] && i<begin[b1]) {
    std::ptrdiff_t b = (b0 + b1)/2;
    assert(b>b0 && b<b1);
    if (i >= begin[b])
      b0 = b;
    else
      b1 = b;
    assert(loopinv());
    auto next_loopvar = loopvar();
    assert(next_loopvar >= 0 && next_loopvar < old_loopvar);
    old_loopvar = next_loopvar;
  }
  assert(loopinv());
  return b0;
}

std::ostream& operator<<(std::ostream& os, const structure_t& str)
{
  os << "{";
  for (std::ptrdiff_t b=0; b<str.B; ++b) {
    if (b != 0) os << ",";
    os << str.begin[b];
  }
  os << "}";
  return os;
}



block_vector_t::block_vector_t(const structure_t::const_ptr& str):
  str(str), has_block_(str->B, false), blocks(str->B)
{
}

void block_vector_t::make_block(std::ptrdiff_t b)
{
  assert(b>=0 && b<str->B);
  assert(!has_block(b));
  has_block_[b] = true;
  blocks[b].create(str->locs[b], str->size(b));
}

void block_vector_t::remove_block(std::ptrdiff_t b)
{
  assert(b>=0 && b<str->B);
  assert(has_block(b));
  has_block_[b] = false;
  blocks[b] = vector_t_client();
}

std::ostream& operator<<(std::ostream& os, const block_vector_t& x)
{
  os << "{";
  for (std::ptrdiff_t b=0; b<x.str->B; ++b) {
    if (b != 0) os << ",";
    os << x.str->begin[b];
    if (x.has_block(b)) {
      os << ":" << *x.block(b).get_data().get();
    }
  }
  os << "}";
  return os;
}



block_matrix_t::block_matrix_t(const structure_t::const_ptr& istr,
                               const structure_t::const_ptr& jstr):
  istr(istr), jstr(jstr),
  has_block_(istr->B*jstr->B, false), blocks(istr->B*jstr->B)
{
}

void block_matrix_t::make_block(std::ptrdiff_t ib, std::ptrdiff_t jb)
{
  assert(ib>=0 && ib<istr->B && jb>=0 && jb<jstr->B);
  assert(!has_block(ib,jb));
  auto b = ib+istr->B*jb;
  has_block_[b] = true;
  blocks[b].create(istr->locs[ib], istr->size(ib),jstr->size(jb));
}

void block_matrix_t::remove_block(std::ptrdiff_t ib, std::ptrdiff_t jb)
{
  assert(ib>=0 && ib<istr->B && jb>=0 && jb<jstr->B);
  assert(has_block(ib,jb));
  auto b = ib+istr->B*jb;
  has_block_[b] = false;
  blocks[b] = matrix_t_client();
}

std::ostream& operator<<(std::ostream& os, const block_matrix_t& a)
{
  os << "{";
  for (std::ptrdiff_t ib=0; ib<a.istr->B; ++ib) {
    if (ib != 0) os << ",";
    os << "{";
    for (std::ptrdiff_t jb=0; jb<a.jstr->B; ++jb) {
      if (jb != 0) os << ",";
      os << "(" << a.istr->begin[ib] << "," << a.jstr->begin[jb] << ")";
      if (a.has_block(ib,jb)) {
        os << ":" << *a.block(ib,jb).get_data().get();
      }
    }
    os << "}";
  }
  os << "}";
  return os;
}
