#ifndef REDUCTION_HH
#define REDUCTION_HH

#include <hpx/hpx.hpp>

#include <cassert>
#include <cstdlib>
#include <vector>



namespace {
  template<typename F, typename T>
  T freduce(const F& op,
            const std::vector<T>& xs,
            typename std::vector<T>::const_iterator xbegin,
            typename std::vector<T>::const_iterator xend)
  {
    const auto sz = xend - xbegin;
    assert(sz > 0);
    if (sz == 1) return *xbegin;
    const auto sz1 = (sz+1)/2;
    const auto sz2 = sz - sz1;
    assert(sz1 > 0 && sz1 <= (sz+1)/2);
    assert(sz2 > 0 && sz2 <= (sz+1)/2);
    assert(xbegin + sz1 == xend - sz2);
    const T x1 = freduce(op, xs, xbegin, xbegin + sz1);
    const T x2 = freduce(op, xs, xend - sz2, xend);
    const T xres = op(x1, x2);
    return xres;
  }
}

template<typename F, typename T>
T freduce(const F& op, const std::vector<T>& xs)
{
  return freduce(op, xs, xs.cbegin(), xs.cend());
}



namespace {
  template<typename F, typename G, typename T, typename U>
  T freduce(const F& preprocess,
            const G& combine,
            const T& y0 __attribute__((__unused__)),
            typename std::vector<U>::const_iterator xbegin,
            typename std::vector<U>::const_iterator xend)
  {
    const auto sz = xend - xbegin;
    assert(sz > 0);
    if (sz == 1) {
      const U& x = *xbegin;
      const T y = preprocess(x);
      return y;
    }
    const auto sz1 = (sz+1)/2;
    const auto sz2 = sz - sz1;
    assert(sz1 > 0 && sz1 <= (sz+1)/2);
    assert(sz2 > 0 && sz2 <= (sz+1)/2);
    assert(xbegin + sz1 == xend - sz2);
    const T y1 = freduce(preprocess, combine, y0, xbegin, xbegin + sz1);
    const T y2 = freduce(preprocess, combine, y0, xend - sz2, xend);
    const T yres = combine(y1, y2);
    return yres;
  }
}

template<typename F, typename G, typename T, typename U>
T freduce(const F& preprocess, const G& combine,
          const T& y0, const std::vector<U>& xs)
{
  if (xs.empty()) return y0;
  return freduce(preprocess, combine, y0, xs.cbegin(), xs.cend());
}



template<typename F, typename G, typename T, typename U>
struct freduce_t {
  typedef typename std::vector<hpx::future<T>>::const_iterator const_iterator;
  const F& preprocess;
  const G& combine;
  
  freduce_t(const F& preprocess, const G& combine):
    preprocess(preprocess), combine(combine)
  {
  }
  
  auto operator()(const_iterator xbegin, const_iterator xend) const ->
    hpx::future<U>
  {
    const auto sz = xend - xbegin;
    assert(sz > 0);
    if (sz == 1) {
      const hpx::future<T>& x = *xbegin;
      const hpx::future<U> y = hpx::async(preprocess, x);
      return y;
    }
    const auto sz1 = (sz+1)/2;
    const auto sz2 = sz - sz1;
    assert(sz1 > 0 && sz1 <= (sz+1)/2);
    assert(sz2 > 0 && sz2 <= (sz+1)/2);
    assert(xbegin + sz1 == xend - sz2);
    const hpx::future<T> y1 = (*this)(xbegin, xbegin + sz1);
    const hpx::future<T> y2 = (*this)(xend - sz2, xend);
    const hpx::future<T> yres = combine(y1, y2);
    return yres;
  }
};

template<typename F, typename G, typename T, typename U>
auto freduce(const F& preprocess, const G& combine,
             const hpx::future<U>& y0, const std::vector<hpx::future<T>>& xs) ->
  hpx::future<U>
{
  if (xs.empty()) return y0;
  return freduce_t<F,G,T,U>(preprocess, combine)(xs.cbegin(), xs.cend());
}

#endif  // #ifndef REDUCTION_HH
