#include "bench.hh"

#include "algorithms.hh"
#include "algorithms_fun.hh"
#include "block_matrix.hh"
#include "matrix.hh"

#include <hpx/hpx.hpp>
#include <hpx/lcos/broadcast.hpp>

#include <algorithm>
#include <cmath>
#include <utility>



namespace bench {
  std::ptrdiff_t niters;
  std::ptrdiff_t nsize;
  std::ptrdiff_t nblocks;
}
using namespace bench;



template<typename T>
static T div_down(T x, T y)
{
  assert(x>=0);
  assert(y>0);
  return x / y;
}

template<typename T>
static T div_up(T x, T y)
{
  assert(x>=0);
  assert(y>0);
  return (x + y - 1) / y;
}

template<typename T>
static T align_up(T x, T y)
{
  assert(x>=0);
  assert(y>0);
  return y * div_up(x, y);
}

template<typename T>
static T align_down(T x, T y)
{
  assert(x>=0);
  assert(y>0);
  return y * div_down(x, y);
}



typedef std::pair<double,double> result_t;



result_t run_dense_bench(std::ptrdiff_t n)
{
  matrix_t a(n,n), b(n,n), c(n,n);
  set(1.0, a);
  set(1.0, b);
  set(1.0, c);
  double res = 0.0;
  
  // Warmup
  gemm(false, false, 1.0, a, b, 1.0, c);
  res += nrm2(c);
  
  // Benchmark
  const auto t0 = gettime();
  for (int iter=0; iter<niters; ++iter) {
    gemm(false, false, 1.0, a, b, 1.0, c);
    res += nrm2(c);
  }
  const auto t1 = gettime();
  
  // Cooldown
  gemm(false, false, 1.0, a, b, 1.0, c);
  res += nrm2(c);
  
  const double t = elapsed(t1, t0), u = niters;
  return { t, u };
}
HPX_PLAIN_ACTION(run_dense_bench);
HPX_REGISTER_BROADCAST_ACTION_DECLARATION(run_dense_bench_action);
HPX_REGISTER_BROADCAST_ACTION(run_dense_bench_action);



result_t run_dense_fbench(std::ptrdiff_t n)
{
  auto a = boost::make_shared<matrix_t>(n,n);
  auto b = boost::make_shared<matrix_t>(n,n);
  auto c = boost::make_shared<matrix_t>(n,n);
  set(1.0, *a);
  set(1.0, *b);
  set(1.0, *c);
  double res = 0.0;
  
  // Warmup
  c = fgemm(false, false, false, 1.0, a, b, 1.0, c);
  res += fnrm2(c);
  
  // Benchmark
  const auto t0 = gettime();
  for (int iter=0; iter<niters; ++iter) {
    c = fgemm(false, false, false, 1.0, a, b, 1.0, c);
    res += fnrm2(c);
  }
  const auto t1 = gettime();
  
  // Cooldown
  c = fgemm(false, false, false, 1.0, a, b, 1.0, c);
  res += fnrm2(c);
  
  const double t = elapsed(t1, t0), u = niters;
  return { t, u };
}
HPX_PLAIN_ACTION(run_dense_fbench);
HPX_REGISTER_BROADCAST_ACTION_DECLARATION(run_dense_fbench_action);
HPX_REGISTER_BROADCAST_ACTION(run_dense_fbench_action);



result_t run_block_fbench(std::ptrdiff_t n, std::ptrdiff_t nb,
                          bool run_global = false)
{
  std::vector<std::ptrdiff_t> begin(nb+1);
  for (std::ptrdiff_t b=0; b<nb+1; ++b) {
    begin[b] = std::min(n, b * div_up(n, nb));
  }
  std::vector<hpx::id_type> locs(nb);
  if (!run_global) {
    const hpx::id_type here = hpx::find_here();
    for (std::ptrdiff_t b=0; b<nb; ++b) locs[b] = here;
  } else {
    const std::vector<hpx::id_type> all_locs = hpx::find_all_localities();
    const size_t nlocs = all_locs.size();
    for (std::ptrdiff_t b=0; b<nb; ++b) locs[b] = all_locs[b % nlocs];
  }
  auto str = boost::make_shared<structure_t>(n, nb, &begin[0], &locs[0]);
  
  auto a = boost::make_shared<block_matrix_t>(str,str);
  auto b = boost::make_shared<block_matrix_t>(str,str);
  auto c = boost::make_shared<block_matrix_t>(str,str);
  a = fset(false, 1.0, a);
  b = fset(false, 1.0, b);
  c = fset(false, 1.0, c);
  double res = 0.0;
  
  // Warmup
  c = fgemm(false, false, false, 1.0, a, b, 1.0, c);
  res += fnrm2(c);
  
  // Benchmark
  const auto t0 = gettime();
  for (int iter=0; iter<niters; ++iter) {
    c = fgemm(false, false, false, 1.0, a, b, 1.0, c);
    res += fnrm2(c);
  }
  const auto t1 = gettime();
  
  const double t = elapsed(t1, t0), u = niters;
  return { t, u };
}
HPX_PLAIN_ACTION(run_block_fbench);
HPX_REGISTER_BROADCAST_ACTION_DECLARATION(run_block_fbench_action);
HPX_REGISTER_BROADCAST_ACTION(run_block_fbench_action);



HPX_PLAIN_ACTION(hpx::get_os_thread_count,
                 hpx__get_os_thread_count_action);
HPX_REGISTER_BROADCAST_ACTION_DECLARATION(hpx__get_os_thread_count_action);
HPX_REGISTER_BROADCAST_ACTION(hpx__get_os_thread_count_action);

std::vector<hpx::id_type> find_all_threads()
{
  const auto locs = hpx::find_all_localities();
  const auto fnthreads =
    hpx::lcos::broadcast(hpx__get_os_thread_count_action(), locs).get();
  std::vector<hpx::id_type> threads;
  for (std::size_t i=0; i<locs.size(); ++i) {
    const auto& loc = locs[i];
    for (std::size_t j=0; j<fnthreads[i]; ++j) {
      threads.push_back(loc);
    }
  }
  if (threads.size() != hpx::get_num_worker_threads()) {
    std::cout << "threads.size()=" << threads.size() << std::endl
              << "hpx::get_num_worker_threads()=" << hpx::get_num_worker_threads() << std::endl
              << std::flush;
  }
  assert(threads.size() == hpx::get_num_worker_threads());
  return threads;
}



void bench_dense()
{
  const std::ptrdiff_t nthreads = 1;
  
  std::cout << "bench_dense N=" << nsize << std::endl;
  
  const result_t res = run_dense_bench(nsize);
  const double t = res.first, u = res.second;
  const double tavg = t / u;
  std::cout << "time for 1 * DGEMM[N=" << nsize << "]: "
            << tavg << " sec" << std::endl;
  
  std::cout << "This run used " << nthreads << " threads" << std::endl;
  
  const double ops = 2.0 * std::pow(double(nsize), 3.0);
  const double mem = 3.0 * std::pow(double(nsize), 2.0) * sizeof(double);
  const double gflop = ops / 1.0e+9;
  const double gbyte = mem / 1.0e+9;
  std::cout << "GFlop/core:     " << gflop << std::endl;
  std::cout << "GByte/core:     " << gbyte << std::endl;
  std::cout << "GFlop/sec/core: " << gflop / tavg << std::endl;
  std::cout << "GByte/sec/core: " << gbyte / tavg << std::endl;
  
  std::cout << std::endl;
  std::cout << std::flush;
}

void bench_fdense()
{
  const std::ptrdiff_t nthreads = 1;
  
  std::cout << "bench_fdense N=" << nsize << std::endl;
  
  const result_t res = run_dense_fbench(nsize);
  const double t = res.first, u = res.second;
  const double tavg = t / u;
  std::cout << "time for 1 * DGEMM[N=" << nsize << "]: "
            << tavg << " sec" << std::endl;
  
  std::cout << "This run used " << nthreads << " threads" << std::endl;
  
  const double ops = 2.0 * std::pow(double(nsize), 3.0);
  const double mem = 3.0 * std::pow(double(nsize), 2.0) * sizeof(double);
  const double gflop = ops / 1.0e+9;
  const double gbyte = mem / 1.0e+9;
  std::cout << "GFlop/core:     " << gflop << std::endl;
  std::cout << "GByte/core:     " << gbyte << std::endl;
  std::cout << "GFlop/sec/core: " << gflop / tavg << std::endl;
  std::cout << "GByte/sec/core: " << gbyte / tavg << std::endl;
  
  std::cout << std::endl;
  std::cout << std::flush;
}

void bench_dense_parallel()
{
  const auto threads = find_all_threads();
  const auto nthreads = threads.size();
  
  std::cout << "bench_dense_parallel N=" << nsize << std::endl;
  
  const auto results =
    hpx::lcos::broadcast(run_dense_bench_action(), threads, nsize).get();
  result_t res = { 0.0, 0.0 };
  for (const auto& ires: results) {
    res.first += ires.first;
    res.second += ires.second;
  }
  
  const double t = res.first, u = res.second;
  const double tavg = t / u;
  std::cout << "time for " << nthreads << " * DGEMM[N=" << nsize << "]: "
            << tavg << " sec" << std::endl;
  
  std::cout << "This run used " << nthreads << " threads" << std::endl;
  
  const double ops = 2.0 * std::pow(double(nsize), 3.0);
  const double mem = 3.0 * std::pow(double(nsize), 2.0) * sizeof(double);
  const double gflop = ops / 1.0e+9;
  const double gbyte = mem / 1.0e+9;
  std::cout << "GFlop/core:     " << gflop << std::endl;
  std::cout << "GByte/core:     " << gbyte << std::endl;
  std::cout << "GFlop/sec/core: " << gflop / tavg << std::endl;
  std::cout << "GByte/sec/core: " << gbyte / tavg << std::endl;
  
  std::cout << std::endl;
  std::cout << std::flush;
}

void bench_fdense_parallel()
{
  const auto threads = find_all_threads();
  const auto nthreads = threads.size();
  
  std::cout << "bench_fdense_parallel N=" << nsize << std::endl;
  
  const auto results =
    hpx::lcos::broadcast(run_dense_fbench_action(), threads, nsize).get();
  result_t res = { 0.0, 0.0 };
  for (const auto& ires: results) {
    res.first += ires.first;
    res.second += ires.second;
  }
  
  const double t = res.first, u = res.second;
  const double tavg = t / u;
  std::cout << "time for " << nthreads << " * DGEMM[N=" << nsize << "]: "
            << tavg << " sec" << std::endl;
  
  std::cout << "This run used " << nthreads << " threads" << std::endl;
  
  const double ops = 2.0 * std::pow(double(nsize), 3.0);
  const double mem = 3.0 * std::pow(double(nsize), 2.0) * sizeof(double);
  const double gflop = ops / 1.0e+9;
  const double gbyte = mem / 1.0e+9;
  std::cout << "GFlop/core:     " << gflop << std::endl;
  std::cout << "GByte/core:     " << gbyte << std::endl;
  std::cout << "GFlop/sec/core: " << gflop / tavg << std::endl;
  std::cout << "GByte/sec/core: " << gbyte / tavg << std::endl;
  
  std::cout << std::endl;
  std::cout << std::flush;
}



void bench_fblock_local()
{
  const std::ptrdiff_t nthreads = hpx::get_os_thread_count();
  const std::ptrdiff_t nsize1 = std::lrint(nsize * std::sqrt(double(nthreads)));
  
  std::cout << "bench_fblock_local N=" << nsize1 << " B=" << nblocks << std::endl;
  
  const auto t0 = gettime();
  const result_t res = run_block_fbench(nsize1, nblocks);
  const auto t1 = gettime();
  const double t = res.first, u = res.second;
  const double tavg = t / u;
  std::cout << "total benchmark time: " << (t1-t0) << " sec" << std::endl;
  std::cout << "time for 1 * DGEMM[N=" << nsize1 << ",B=" << nblocks << "]: "
            << tavg << " sec" << std::endl;
  
  std::cout << "This run used " << nthreads << " threads" << std::endl;
  
  const double ops = 2.0 * std::pow(double(nsize1), 3.0);
  const double mem = 3.0 * std::pow(double(nsize1), 2.0) * sizeof(double);
  const double gflop = ops / nthreads / 1.0e+9;
  const double gbyte = mem / nthreads / 1.0e+9;
  std::cout << "GFlop/core:     " << gflop << std::endl;
  std::cout << "GByte/core:     " << gbyte << std::endl;
  std::cout << "GFlop/sec/core: " << gflop / tavg << std::endl;
  std::cout << "GByte/sec/core: " << gbyte / tavg << std::endl;
  
  std::cout << std::endl;
  std::cout << std::flush;
}

void bench_fblock_global()
{
  const std::ptrdiff_t nthreads = hpx::get_num_worker_threads();
  const std::ptrdiff_t nsize1 = std::lrint(nsize * std::sqrt(double(nthreads)));
  
  std::cout << "bench_fblock_global N=" << nsize1 << " B=" << nblocks
            << std::endl;
  
  const auto t0 = gettime();
  const result_t res = run_block_fbench(nsize1, nblocks, true);
  const auto t1 = gettime();
  const double t = res.first, u = res.second;
  const double tavg = t / u;
  std::cout << "total benchmark time: " << (t1-t0) << " sec" << std::endl;
  std::cout << "time for 1 * DGEMM[N=" << nsize1 << ",B=" << nblocks << "]: "
            << tavg << " sec" << std::endl;
  
  std::cout << "This run used " << nthreads << " threads" << std::endl;
  
  const double ops = 2.0 * std::pow(double(nsize1), 3.0);
  const double mem = 3.0 * std::pow(double(nsize1), 2.0) * sizeof(double);
  const double gflop = ops / nthreads / 1.0e+9;
  const double gbyte = mem / nthreads / 1.0e+9;
  std::cout << "GFlop/core:     " << gflop << std::endl;
  std::cout << "GByte/core:     " << gbyte << std::endl;
  std::cout << "GFlop/sec/core: " << gflop / tavg << std::endl;
  std::cout << "GByte/sec/core: " << gbyte / tavg << std::endl;
  
  std::cout << std::endl;
  std::cout << std::flush;
}
