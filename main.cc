#include "bench.hh"
#include "defs.hh"
#include "hpxbench.hh"
#include "hwloc.hh"
#include "tests.hh"

#include <hpx/hpx.hpp>
#include <hpx/hpx_init.hpp>

#include <iostream>



void output_hpx_info()
{
  std::cout << "HPX information:" << std::endl;
  
  int nlocs = hpx::get_num_localities().get();
  std::cout << "There are " << nlocs << " localities" << std::endl;
  hpx::id_type here = hpx::find_here();
  std::cout << "   here=" << here << std::endl;
  hpx::id_type root = hpx::find_root_locality();
  std::cout << "   root=" << root << std::endl;
  std::vector<hpx::id_type> locs = hpx::find_all_localities();
  std::cout << "   locs=" << mkstr(locs) << std::endl;
  
  int nthreads = hpx::get_num_worker_threads();
  std::cout << "There are " << nthreads << " threads overall" << std::endl;
  hpx::threads::thread_id_type self = hpx::threads::get_self_id();
  std::string name = hpx::get_thread_name();
  std::cout << "   self=" << self << " name=" << name << std::endl;
  
  std::cout << std::endl;
  std::cout << std::flush;
}



HPX_PLAIN_ACTION(test_dense);
HPX_PLAIN_ACTION(test_block);

int hpx_main(boost::program_options::variables_map&)
{
  std::cout << "Block-structured DGEMM with HPX" << std::endl
            << "2013-09-11 Erik Schnetter <eschnetter@perimeterinstitute.ca>" << std::endl
            << std::endl;
  
  init_timebase();
  output_hpx_info();
  // TODO: Re-enable once the scheduler works
  output_hwloc_info();
  hpxbench();
  
#ifdef CBLAS
  std::cout << "Using CBLAS" << std::endl;
#  if defined GSL_CBLAS
  std::cout << "   Using CBLAS via GSL" << std::endl;
#  elif defined MKL_CBLAS
  std::cout << "   Using CBLAS via MKL" << std::endl;
#  else
  std::cout << "   Using CBLAS directly" << std::endl;
#  endif
#else
  std::cout << "Not using CBLAS" << std::endl;
#endif
  
#if 1
  // Direct function calls
  test_dense();
  test_block();
#elif 0
  // Async function calls
  hpx::future<void> f1 = hpx::async(test_dense);
  hpx::future<void> f2 = hpx::async(test_block);
  hpx::wait_all(f1, f2);
#elif 0
  // Async action calls
  hpx::id_type here = hpx::find_here();
  hpx::future<void> f1 = hpx::async(test_dense_action(), here);
  hpx::future<void> f2 = hpx::async(test_block_action(), here);
  hpx::wait_all(f1, f2);
#elif 1
  // Async action calls on remote localities
  int nlocs = hpx::get_num_localities().get();
  std::vector<hpx::id_type> locs = hpx::find_all_localities();
  hpx::id_type loc1 = locs[1 % nlocs];
  hpx::id_type loc2 = locs[2 % nlocs];
  hpx::future<void> f1 = hpx::async(test_dense_action(), loc1);
  hpx::future<void> f2 = hpx::async(test_block_action(), loc2);
  hpx::wait_all(f1, f2);
#endif
  
  bench_dense();
  bench_fdense();
  bench_dense_parallel();
  bench_fdense_parallel();
  bench_fblock_local();
  bench_fblock_global();
  
  std::cout << "Done." << std::endl;
  std::cout << std::flush;
  
  return hpx::finalize();
}



int main(int argc, char** argv)
{
  // Configure application-specific options
  namespace po = boost::program_options;
  po::options_description desc("Benchmarking options");
  desc.add_options()
    ("niters",
     po::value<std::ptrdiff_t>(&bench::niters)->default_value(3),
     "number of iterations size")
    ("nsize",
     po::value<std::ptrdiff_t>(&bench::nsize)->default_value(2000),
     "total matrix size")
    ("nblocks",
     po::value<std::ptrdiff_t>(&bench::nblocks)->default_value(10),
     "number of blocks");
  
  return hpx::init(desc, argc, argv);
}
